# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class UnitLoadTestCase(ModuleTestCase):
    """Test unit load business process module"""
    module = 'stock_unit_load_business_process'

    def setUp(self):
        super(UnitLoadTestCase, self).setUp()


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitLoadTestCase))
    return suite
